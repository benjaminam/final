// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDpeAa59HqSefAPYcFT_fUqbl_lFa0FFjg",
    authDomain: "final-62a77.firebaseapp.com",
    projectId: "final-62a77",
    storageBucket: "final-62a77.appspot.com",
    messagingSenderId: "988035865932",
    appId: "1:988035865932:web:9f743d7495392cde0943e9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
