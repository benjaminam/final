import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { City } from './interfaces/city';
import { WeatherRaw } from './interfaces/weather-raw';


@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  cityCollection:AngularFirestoreCollection = this.db.collection('cities');
  API:string = "https://7jntcj7i6f.execute-api.us-east-1.amazonaws.com/default";
  constructor(private http:HttpClient,private db:AngularFirestore) { }

  predict(temperature,humidity){
    const body = {
      data:{
        temperature:temperature, 
        humidity:humidity
      }
    }
    return this.http.post<any>(this.API,body).pipe(
      map(res => {
        const final:string = res.body;
        console.log({final});
        return final;
      })
    )
  }

  
 

  saveCity(name:string){
    const city = {
      name:name,
    }
    this.cityCollection.add(city);
    return this.cityCollection.snapshotChanges();
  }

 
  deleteCity2(id){
    this.cityCollection.doc(id).delete();
    return this.cityCollection.snapshotChanges();
  }
   


  SavePred(id:string,name:string,temperature:number,humidity:number,wind:number,predict:string,owner:string){
    this.db.doc(`cities/${id}`).update(
      {
        name:name,
        temperature:temperature,
        humidity:humidity,
        wind:wind,
        predict:predict,
        saved:true,
        owner:owner
      }
    )
  }
  
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "86d979390316df3c12488c8da5e7f633";
  private IMP = "units=metric";


  searchWeatherData(cityName:string):Observable<City>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

private handleError(res:HttpErrorResponse){
  console.log(res.error);
  return throwError(res.error || 'Server Error');
}

private transformWeatherData(data:WeatherRaw):City{
return {
  name:data.name,
  image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
  temperature:data.main.temp,
  humidity:data.main.humidity,
  wind:data.wind.speed


}
}





  getCities(){
   
    this.cityCollection = this.db.collection('cities');
    return this.cityCollection.snapshotChanges();
  }



    
}
