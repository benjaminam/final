import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addcity',
  templateUrl: './addcity.component.html',
  styleUrls: ['./addcity.component.css']
})
export class AddcityComponent implements OnInit {

  constructor(private PredictionService:PredictionService,private router:Router) { }
  name:string;


  saveCity(){
    this.PredictionService.saveCity(this.name).subscribe(
      res => this.router.navigate(['/cities'])
    )
 }

  ngOnInit(): void {
  }

}
