import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from '../interfaces/city';
import { AuthService } from './../auth.service';
import { PredictionService } from './../prediction.service';

@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {
  cities$:Observable<any>;
  cities:City[] = []
  displayedColumns: string[] = ['name','getdata','temperature','humidity','windspeed','result','delete'];
  owner: string;
  preds:City[] = [];
  preds$:any;


  constructor(private PredictionService:PredictionService,private AuthService:AuthService) { }
  
     deleteCity2(id:string){
    console.log(id) 
    this.PredictionService.deleteCity2(id);
   }


  ngOnInit(): void{
    this.AuthService.getUser().subscribe(
      user => {
        this.owner=user.email;
      });
    this.preds$.subscribe(
      docs => {
        this.preds =[];
        for(let document of docs){
          const city:City = document.payload.doc.data();
          console.log(city);
          city.id = document.payload.doc.id;

          this.preds.push(city);
        }
      }
    )
  }
}
