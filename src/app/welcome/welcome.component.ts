import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  userActiveEmail:string;

  constructor(private auth:AuthService) { }

  ngOnInit(): void {
      this.auth.getUser().subscribe(
        res => this.userActiveEmail =  res.email
      )
     
    }
  
  }