import { SavedComponent } from './saved/saved.component';
import { CitiesComponent } from './cities/cities.component';
import { AddcityComponent } from './addcity/addcity.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ByeComponent } from './bye/bye.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'welcome', component: WelcomeComponent},
  { path: 'signup', component: SignUpComponent}, 
  { path: 'bye', component: ByeComponent},
  { path: 'addcity', component: AddcityComponent}, 
  { path: 'cities', component: CitiesComponent}, 
  { path: 'saved', component: SavedComponent}, 
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
