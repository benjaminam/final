import { AuthService } from './../auth.service';
import { PredictionService } from './../prediction.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { City } from '../interfaces/city';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  cities$:Observable<any>;
  cities:City[] = []
  displayedColumns: string[] = ['name','getdata','temperature','humidity','windspeed','image','predict','result','delete'];
  weatherData$: any;
  hasError: boolean;
  errorMessage: any;
  city: string;
  result$:any;
  preds$:any;
  owner: any;
  preds:City[] = [];
  ISSAVED:boolean=false




  constructor(private PredictionService:PredictionService,private AuthService:AuthService) { }

  predict(index){
    // console.log(index);
    // console.log(this.cities[index]);
    this.result$=this.PredictionService.predict(this.cities[index].temperature,this.cities[index].humidity);
    this.result$=this.result$.subscribe(
      result=>{
        console.log(result);
        if (result>50){
          this.cities[index].predict = "rain";
        }
        else {
          this.cities[index].predict = "no rain";
        }


      }
      
    )
  }


  // deleteCity(id:string){
  // //  console.log(owner) 
  //  this.PredictionService.deleteCity(id);
  // }
  deleteCity2(id:string){
    console.log(id) 
    this.PredictionService.deleteCity2(id);
   }

//   addPredict(index){
//     this.PredictionService.addPredict(this.cities[index].name,this.cities[index].temperature,this.cities[index].humidity,this.cities[index].wind,this.cities[index].predict,this.owner); 
//   // this.cities[index].saved = true;
// }
  getDataFun(index, name){
    console.log(name);
    
    this.weatherData$=this.PredictionService.searchWeatherData(name).subscribe(
      data => {
        console.log(data);
        this.cities[index].temperature = data.temperature;
        this.cities[index].image = data.image;
        this.cities[index].humidity = data.humidity;
        this.cities[index].wind = data.wind;
        // this.cities[index].saved = true;
        this.city = name;
     
      },
      error=>{
        console.log(error.message);
        this.hasError= true;
        this.errorMessage=error.message;
      }
    )
  }

  updateResult(index){
    this.PredictionService.SavePred(this.cities[index].id,this.cities[index].name,this.cities[index].temperature,this.cities[index].humidity,this.cities[index].wind,this.cities[index].predict,this.owner);
    this.cities[index].saved = true; 
  }
  ngOnInit(): void {
    this.AuthService.getUser().subscribe(
      user => {
        this.owner=user.email;
      });
    this.cities$ = this.PredictionService.getCities();
    this.cities$.subscribe(
      docs => {
        this.cities =[];
        for(let document of docs){
          const city:City = document.payload.doc.data();
          console.log(city);
          city.id = document.payload.doc.id;
          // city.saved=false;
//           this.cities.forEach(elCity =>{
//             this.preds =[];
// this.preds.forEach(elPred=>{if (elCity.name === city.name){
//   elCity.humidity = city.humidity;
//   elCity.temperature = city.temperature;
//   elCity.wind = city.wind;
//   elCity.predict = city.predict;
//   elCity.saved = true;
// }
// })
//             })
          this.cities.push(city);
        }
      }
    )
  }

}
